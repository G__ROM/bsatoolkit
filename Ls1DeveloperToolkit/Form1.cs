﻿using System.IO;
using System.Windows.Forms;

namespace Ls1DeveloperToolkit
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            using (var pluginManager = new PluginManager())
            {
                try
                {
                    var plugins = pluginManager.GetPlugins(string.Empty);
                    foreach (var plugin in plugins)
                    {
                        plugin.LoadPlugin(tabControl1);
                    }
                }
                catch (DirectoryNotFoundException exception)
                {
                    MessageBox.Show(exception.Message, exception.Source);
                }
            }
        }
    }
}
