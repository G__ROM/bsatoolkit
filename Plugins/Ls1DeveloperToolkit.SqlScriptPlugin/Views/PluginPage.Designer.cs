﻿namespace Ls1DeveloperToolkit.SqlScriptPlugin.Views
{
    partial class PluginPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scriptTemplateListView = new System.Windows.Forms.ListView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.scriptTemplateContent = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scriptTemplateListView
            // 
            this.scriptTemplateListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptTemplateListView.Location = new System.Drawing.Point(0, 0);
            this.scriptTemplateListView.MultiSelect = false;
            this.scriptTemplateListView.Name = "scriptTemplateListView";
            this.scriptTemplateListView.Size = new System.Drawing.Size(167, 325);
            this.scriptTemplateListView.TabIndex = 0;
            this.scriptTemplateListView.UseCompatibleStateImageBehavior = false;
            this.scriptTemplateListView.View = System.Windows.Forms.View.List;
            this.scriptTemplateListView.SelectedIndexChanged += new System.EventHandler(this.scriptTemplateListView_SelectedIndexChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.scriptTemplateListView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.scriptTemplateContent);
            this.splitContainer1.Size = new System.Drawing.Size(502, 325);
            this.splitContainer1.SplitterDistance = 167;
            this.splitContainer1.TabIndex = 1;
            // 
            // scriptTemplateContent
            // 
            this.scriptTemplateContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scriptTemplateContent.Location = new System.Drawing.Point(3, 3);
            this.scriptTemplateContent.Multiline = true;
            this.scriptTemplateContent.Name = "scriptTemplateContent";
            this.scriptTemplateContent.Size = new System.Drawing.Size(325, 319);
            this.scriptTemplateContent.TabIndex = 0;
            // 
            // PluginPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PluginPage";
            this.Size = new System.Drawing.Size(502, 325);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView scriptTemplateListView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox scriptTemplateContent;
    }
}
