﻿using System.Collections.Generic;

namespace Ls1DeveloperToolkit.SqlScriptPlugin.Interfaces
{
    public interface IScriptTemplateService
    {
        IList<string> GetScriptTemplatesList();

        string GetScriptTemplateConent(string resourceName);
    }
}
