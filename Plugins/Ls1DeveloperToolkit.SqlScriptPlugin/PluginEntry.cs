﻿using System.Windows.Forms;

using Ninject;
using Ninject.Modules;

using Ls1DeveloperToolkit.SqlScriptPlugin.Interfaces;
using Ls1DeveloperToolkit.SqlScriptPlugin.Services;
using Ls1DeveloperToolkit.SqlScriptPlugin.Views;

namespace Ls1DeveloperToolkit.SqlScriptPlugin
{
    public class PluginNinjectModule : NinjectModule
    {
        /// <summary>Loads the module into the kernel.</summary>
        public override void Load()
        {
            Bind<IScriptTemplateService>().To<ScriptTemplateService>();
        }
    }

    public class PluginEntry : BasePlugin.BasePlugin
    {
        private UserControl pluginPage;

        public override string PluginVisibleName => "SQL script generator";

        public override UserControl PluginPage => pluginPage ?? (pluginPage = new PluginPage(new StandardKernel(new PluginNinjectModule())));
    }
}
