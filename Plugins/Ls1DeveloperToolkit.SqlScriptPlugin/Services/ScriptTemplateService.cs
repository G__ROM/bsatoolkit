﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Ls1DeveloperToolkit.SqlScriptPlugin.Interfaces;

namespace Ls1DeveloperToolkit.SqlScriptPlugin.Services
{
    public class ScriptTemplateService : IScriptTemplateService
    {
        public IList<string> GetScriptTemplatesList()
        {
            IList<string> result = new List<string>();

            foreach (var name in Assembly.GetExecutingAssembly().GetManifestResourceNames())
            {
                if (!name.EndsWith(".sql")) continue;

                result.Add(name);
            }

            return result;
        }

        public string GetScriptTemplateConent(string resourceName)
        {
            string result = string.Empty;

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }
    }
}
