﻿/*
	Issue No.		: 
	Responsible		: 
	Date created	: 
	Description		: 
*/

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[tablename_DFV_columnname]') AND type = 'D')
BEGIN
	ALTER TABLE [dbo].[tablename] DROP CONSTRAINT [tablename_DFV_columnname]
END