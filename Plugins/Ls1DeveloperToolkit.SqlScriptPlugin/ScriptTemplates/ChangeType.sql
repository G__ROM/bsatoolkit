﻿/*
	Issue No.		: 
	Responsible		: 
	Date created	: 
	Description		: 
*/

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tablename' AND COLUMN_NAME='columnname')
BEGIN
	IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[tablename_DFV_columnname]') AND type = 'D')
	BEGIN
		ALTER TABLE [dbo].[tablename] DROP CONSTRAINT [tablename_DFV_columnname]
	END

	ALTER TABLE [dbo].[tablename] ALTER COLUMN [columnname] newtype
	ALTER TABLE [dbo].[tablename] ADD CONSTRAINT [tablename_DFV_columnname] DEFAULT() FOR [columnname]
END
GO