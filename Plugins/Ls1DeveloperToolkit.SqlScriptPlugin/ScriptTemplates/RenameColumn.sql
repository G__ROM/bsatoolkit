﻿/*
	Issue No.		: 
	Responsible		: 
	Date created	: 
	Description		: 
*/

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tablename' AND COLUMN_NAME='columnname')
BEGIN
	EXEC sp_RENAME 'tablename.columnname' , 'columnname2', 'COLUMN'
END
GO