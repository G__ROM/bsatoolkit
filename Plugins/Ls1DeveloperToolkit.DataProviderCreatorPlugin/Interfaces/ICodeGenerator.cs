﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces
{
    public interface ICodeGenerator
    {
        string GenerateCode(DataProviderInfo dataProviderInfo);
        void SaveToFile(string path);
    }
}
