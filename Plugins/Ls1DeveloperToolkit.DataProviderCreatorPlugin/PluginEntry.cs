﻿using System.Windows.Forms;

using Ninject;
using Ninject.Modules;

using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Views;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin
{
    public class PluginNinjectModule : NinjectModule
    {
        /// <summary>Loads the module into the kernel.</summary>
        public override void Load()
        {
           // Bind<IScriptTemplateService>().To<ScriptTemplateService>();
        }
    }

    public class PluginEntry : BasePlugin.BasePlugin
    {
        private UserControl pluginPage;

        public override string PluginVisibleName => "Data provider generator";

        public override UserControl PluginPage => pluginPage ?? (pluginPage = new PluginPage(new StandardKernel(new PluginNinjectModule())));
    }
}
