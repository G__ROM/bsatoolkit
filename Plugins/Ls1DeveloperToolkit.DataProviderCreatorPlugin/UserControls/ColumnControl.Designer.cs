﻿namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.UserControls
{
    partial class ColumnControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtColName = new System.Windows.Forms.TextBox();
            this.txtPropName = new System.Windows.Forms.TextBox();
            this.chkbPK = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.cmbSqlType = new System.Windows.Forms.ComboBox();
            this.cmbPropType = new System.Windows.Forms.ComboBox();
            this.chkbNullable = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtColName
            // 
            this.txtColName.Location = new System.Drawing.Point(157, 2);
            this.txtColName.Name = "txtColName";
            this.txtColName.Size = new System.Drawing.Size(135, 20);
            this.txtColName.TabIndex = 2;
            this.txtColName.TextChanged += new System.EventHandler(this.Control_ValueChanged);
            this.txtColName.Leave += new System.EventHandler(this.txtColName_Leave);
            // 
            // txtPropName
            // 
            this.txtPropName.Location = new System.Drawing.Point(576, 2);
            this.txtPropName.Name = "txtPropName";
            this.txtPropName.Size = new System.Drawing.Size(135, 20);
            this.txtPropName.TabIndex = 4;
            this.txtPropName.TextChanged += new System.EventHandler(this.Control_ValueChanged);
            // 
            // chkbPK
            // 
            this.chkbPK.AutoSize = true;
            this.chkbPK.Location = new System.Drawing.Point(4, 4);
            this.chkbPK.Name = "chkbPK";
            this.chkbPK.Size = new System.Drawing.Size(40, 17);
            this.chkbPK.TabIndex = 1;
            this.chkbPK.Text = "PK";
            this.chkbPK.UseVisualStyleBackColor = true;
            this.chkbPK.CheckedChanged += new System.EventHandler(this.Control_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ColName";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(308, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "SqlType";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(513, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "PropName";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(721, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "PropType";
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(927, -1);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(39, 23);
            this.btnDel.TabIndex = 6;
            this.btnDel.Text = "DEL";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // cmbSqlType
            // 
            this.cmbSqlType.FormattingEnabled = true;
            this.cmbSqlType.Items.AddRange(new object[] {
            "nvarchar(20)",
            "nvarchar(MAX)",
            "int",
            "tinyint",
            "bit",
            "uniqueidentifier",
            "datetime",
            "float",
            "xml",
            "numeric(28, 12)",
            "geography"});
            this.cmbSqlType.Location = new System.Drawing.Point(361, 2);
            this.cmbSqlType.Name = "cmbSqlType";
            this.cmbSqlType.Size = new System.Drawing.Size(135, 21);
            this.cmbSqlType.TabIndex = 3;
            this.cmbSqlType.TextChanged += new System.EventHandler(this.Control_ValueChanged);
            this.cmbSqlType.Leave += new System.EventHandler(this.cmbSqlType_Leave);
            // 
            // cmbPropType
            // 
            this.cmbPropType.FormattingEnabled = true;
            this.cmbPropType.Items.AddRange(new object[] {
            "bool",
            "string",
            "int",
            "byte",
            "double",
            "Guid",
            "DateTime"});
            this.cmbPropType.Location = new System.Drawing.Point(780, 2);
            this.cmbPropType.Name = "cmbPropType";
            this.cmbPropType.Size = new System.Drawing.Size(135, 21);
            this.cmbPropType.TabIndex = 5;
            this.cmbPropType.TextChanged += new System.EventHandler(this.Control_ValueChanged);
            // 
            // chkbNullable
            // 
            this.chkbNullable.AutoSize = true;
            this.chkbNullable.Checked = true;
            this.chkbNullable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkbNullable.Location = new System.Drawing.Point(47, 4);
            this.chkbNullable.Name = "chkbNullable";
            this.chkbNullable.Size = new System.Drawing.Size(54, 17);
            this.chkbNullable.TabIndex = 8;
            this.chkbNullable.Text = "NULL";
            this.chkbNullable.UseVisualStyleBackColor = true;
            this.chkbNullable.CheckedChanged += new System.EventHandler(this.chkbNullable_CheckedChanged);
            // 
            // ColumnControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkbNullable);
            this.Controls.Add(this.cmbPropType);
            this.Controls.Add(this.cmbSqlType);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkbPK);
            this.Controls.Add(this.txtPropName);
            this.Controls.Add(this.txtColName);
            this.Name = "ColumnControl";
            this.Size = new System.Drawing.Size(976, 29);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtColName;
        private System.Windows.Forms.TextBox txtPropName;
        private System.Windows.Forms.CheckBox chkbPK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.ComboBox cmbSqlType;
        private System.Windows.Forms.ComboBox cmbPropType;
        private System.Windows.Forms.CheckBox chkbNullable;
    }
}
