﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ls1DeveloperToolkit.DataProviderCreatorPlugin.Interfaces;

namespace Ls1DeveloperToolkit.DataProviderCreatorPlugin.CodeGenerators
{
    public class PoresyProviderCodeGenerator : ICodeGenerator
    {
        public PoresyProviderCodeGenerator() { }

        public string GenerateCode(DataProviderInfo dataProviderInfo)
        {
            return $@"/// <summary>
/// {dataProviderInfo.BusinessObject} data provider
/// </summary>
public static I{dataProviderInfo.BusinessObject}Data {dataProviderInfo.BusinessObject}Data => DataProviderFactory.Instance.Get<I{dataProviderInfo.BusinessObject}Data, {dataProviderInfo.BusinessObject}>();";
        }

        public void SaveToFile(string path)
        {

        }
    }
}
