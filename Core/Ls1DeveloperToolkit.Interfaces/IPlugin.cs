﻿using System.Windows.Forms;

namespace Ls1DeveloperToolkit.Interfaces
{
    public interface IPlugin
    {
        /// <summary>
        /// Plugin visible name
        /// </summary>
        string PluginVisibleName { get; }

        /// <summary>
        /// Load plugin tab page method
        /// </summary>
        /// <param name="tabControl">TabControl oject to embed tab</param>
        void LoadPlugin(TabControl tabControl);
    }
}
